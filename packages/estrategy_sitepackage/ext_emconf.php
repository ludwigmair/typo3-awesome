<?php

/**
 * Extension Manager/Repository config file for ext "estrategy_sitepackage".
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'estrategy-sitepackage',
    'description' => '',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
            'bootstrap_package' => '13.0.0-14.9.99',
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'Typopublic\\EstrategySitepackage\\' => 'Classes',
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Ludwig Mair',
    'author_email' => 'services@typopublic.com',
    'author_company' => 'Typopublic',
    'version' => '1.0.0',
];
